module.exports = function(grunt) {

    // Get the tasks in
    grunt.loadNpmTasks('assemble');
    grunt.loadNpmTasks('grunt-contrib-connect');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-notify');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    require('time-grunt')(grunt);

    // List of javascript from bower you want to concatenate
    var jsFromBower = [
    'bower_components/jquery/dist/jquery.js',
    'bower_components/fastclick/lib/fastclick.js',
    'bower_components/owl-carousel/owl-carousel/owl.carousel.js'
    ];

    // List of css from bower you want to concatenate
    var cssFromBower = [
    'bower_components/animate-css/animate.css',
    'bower_components/owlcarousel/owl-carousel/owl.carousel.css',
    'bower_components/owlcarousel/owl-carousel/owl.theme.css',
    'bower_components/owlcarousel/owl-carousel/owl.transitions.css'
    ];

    grunt.initConfig({
        // Get the values from the package.json so we can use them
        pkg: grunt.file.readJSON('package.json'),

        // Now the tasks

        // Check yo'self
        jshint: {
            all: ['app/assets/**/*.js']
        },

        // Clean out the dist
        clean: {
            dev: ['dist'],
            build: ['dist']
        },

        // Copy images / videos etc into the dist
        copy: {
            dev: {
                files: [
                    {
                        expand: true,
                        // Copy all the stuff you want in
                        src: ['downloads/**','videos/**','img/**','fonts/**'],
                        dest: 'dist/assets',
                        cwd: 'app/assets'
                    }
                ]
            },
            build: {
                files: [
                    {
                        expand: true,
                        // Copy all the stuff you want in
                        src: ['downloads/**','videos/**','img/**','fonts/**'],
                        dest: 'dist/assets',
                        cwd: 'app/assets'
                    }
                ]
            }
        },

        // Smush the JS
        uglify: {
            dev: {
                files: {
                    'dist/assets/js/script.js': ['app/assets/js/**/*.js']
                },
                options: {
                    beautify: true,
                    mangle: false
                }
            },
            build: {
                files: {
                    'dist/assets/js/script.js': ['app/assets/js/**/*.js']
                },
                options: {
                    beautify: false,
                    mangle: true,
                    compress: {
                        drop_console: true
                    },
                    banner: '/* <%= grunt.template.today("yyyy-mm-dd") %> */'
                }
            },
        },

        // Crunch all the bower stuff from the list at the top
        concat: {
            options: {
                separator: ';',
            },
            vendor: {
                src: [jsFromBower],
                dest: 'dist/assets/js/vendor.js',
            },
        },

        // CSS
        sass: {
            dev: {
                files: {
                    'dist/assets/css/main.css': 'app/assets/scss/main.scss'
                },
                options: {
                    compress: false
                },
            },
            build: {
                files: {
                    'dist/assets/css/main.css': 'app/assets/scss/main.scss'
                },
                options: {
                    compress: false
                },
            }
        },

        // get all the css you want out of the bower folders
        cssmin: {
                files: {
                    src: [cssFromBower],
                    dest: 'dist/assets/css/vendor.css'
                }
        },


        // Templates
        assemble: {
            options: {
                assets: 'dist/assets',
                partials: ['app/templates/partials/**/*.hbs'],
                layout: ['app/templates/layout/default.hbs'],
                data: ['app/data/*.{json,yml}']
            },
            site: {
                options: {
                    layout: 'app/templates/layout/default.hbs'
                },
                files: [{
                    expand: true,
                    cwd: 'app/templates/pages',
                    src: ['*.hbs'],
                    dest: 'dist/'
                }]
            },
        },

        watch: {
            options: {
                livereload: true
            },
            grunt: {
                files: ['Gruntfile.js'],
                tasks: ['dev'],
                options: {
                    reload: true
                }
            },
            html: {
                files: 'app/templates/**/*.hbs',
                tasks: ['assemble'],
                options: {
                    spawn: false
                }
            },
            assets: {
                files: ['app/assets/**','!app/assets/scss/**/*'],
                tasks: ['copy:dev']
            },
            js: {
                files: 'app/assets/js/**/*.js',
                tasks: ['jshint','uglify:dev']
            },
            sass: {
                options: {
                    livereload: false
                },
                files: 'app/assets/scss/**/*.scss',
                tasks: ['sass:dev'],
            },
            css: {
                files: ['dist/assets/css/*.css'],
                tasks: []
            }
        },

        connect: {
            options: {
                port: 9000,
                livereload: true,
                hostname: 'localhost',
                base: ['dist']
            },
            livereload: {
                options: {
                    open: true
                }
            }
        }
    });

grunt.registerTask('dev', ['clean:dev','copy:dev','assemble','jshint','uglify:dev','concat:vendor','sass:dev','cssmin']);

grunt.registerTask('build', ['clean:build','copy:build','assemble','jshint','uglify:build','concat:vendor','sass:build','cssmin']);

grunt.registerTask('default', ['dev','connect','watch']);

};